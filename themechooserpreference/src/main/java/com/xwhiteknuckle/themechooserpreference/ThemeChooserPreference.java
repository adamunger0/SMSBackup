package com.xwhiteknuckle.themechooserpreference;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.preference.Preference;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class ThemeChooserPreference extends Preference {
    private int[] mPrimaryColors;
    private int[] mHighlightColors;
    private int mDefaultColor;
    private int selectedColor;
    private int colorButtonWidth;
    private LinearLayout parentLayout;
    private ArrayList<CircularImageView> children;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThemeChooserPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public ThemeChooserPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public ThemeChooserPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ThemeChooserPreference(Context context) {
        super(context);
        init(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        if (context != null && attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ThemeChooserPreference);
            mPrimaryColors = context.getResources().getIntArray(a.getResourceId(R.styleable.ThemeChooserPreference_mainColors, 0));
            mHighlightColors = context.getResources().getIntArray(a.getResourceId(R.styleable.ThemeChooserPreference_highlightColors, 0));
            mDefaultColor = a.getResourceId(R.styleable.ThemeChooserPreference_defaultColor, 0);
            a.recycle();
        }
        children = new ArrayList<CircularImageView>(mPrimaryColors.length);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        final LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        parentLayout = (LinearLayout) layoutInflater.inflate(R.layout.theme_chooser_layout, parent, false);
        final ViewTreeObserver observer = parentLayout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                for (int i = 0; i < parentLayout.getChildCount(); ++i) {
                    colorButtonWidth = parentLayout.getHeight();
                    parentLayout.getChildAt(i).setMinimumWidth(colorButtonWidth);
                    parentLayout.getChildAt(i).setMinimumHeight(colorButtonWidth);
                }
            }
        });
        return parentLayout;
    }

    @Override
    protected void onBindView(View view) {
        boolean listCreated = false;
        for (int i = 0; !listCreated && i < mPrimaryColors.length; ++i) {
            CircularImageView cv = createColorSelectView(view, mPrimaryColors[i], mHighlightColors[i]);
            ((LinearLayout) view).addView(cv);
            children.add(cv);
//            listCreated = true;
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return super.onGetDefaultValue(a, index);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        super.onSetInitialValue(restorePersistedValue, defaultValue);
    }

    private CircularImageView createColorSelectView(View parent, int mainColor, int highlightColor) {
        CircularImageView iv = new CircularImageView(getContext());
//        iv.setTag(0, mainColor);
        iv.setMainColor(mainColor);
        iv.setHighlightColor(highlightColor);
        iv.setShowHighlight((mainColor == getContext().getResources().getColor(mDefaultColor)));
        iv.setMinimumWidth(colorButtonWidth);
        iv.setMinimumHeight(colorButtonWidth);
        iv.setFocusable(true);
        iv.setFocusableInTouchMode(true);
        return iv;
    }

    protected void circleClicked(CircularImageView caller) {
        Toast.makeText(getContext(), String.valueOf(children.size()), Toast.LENGTH_SHORT).show();
        for (CircularImageView child : children) {
            child.setShowHighlight(false);
            child.invalidate();
        }
        caller.setShowHighlight(true);
        caller.invalidate();
    }

    public class CircularImageView extends ImageView {
        private float centerX;
        private float centerY;
        private float btnRadius;
        private float highlightThickness;
        private Paint mButtonPaint;
        private Paint mHighlightPaint;
        private boolean showHighlight = false;
        private int mainColor;
        private final CircularImageView me = this;
        private GestureDetectorCompat gestureDetector = new GestureDetectorCompat(getContext(), new mGestureDetector());

        public CircularImageView(Context context) {
            super(context);
            init();
        }

        public CircularImageView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public CircularImageView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init();
        }

        private void init() {
            setWillNotDraw(false);

            mButtonPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mButtonPaint.setStyle(Paint.Style.FILL);

            mHighlightPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mHighlightPaint.setStyle(Paint.Style.FILL);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if (showHighlight) canvas.drawCircle(centerX, centerY, btnRadius, mHighlightPaint);
            else canvas.drawCircle(centerX, centerY, btnRadius, mButtonPaint);
            canvas.drawCircle(centerX, centerY, btnRadius - highlightThickness, mButtonPaint);
        }

        public void setMainColor(int color) {
            mainColor = color;
            mButtonPaint.setColor(color);
        }

        public int getMainColor() {
            return mainColor;
        }

        public void setHighlightColor(int color) {
            mHighlightPaint.setColor(color);
        }

        @Override
        protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
            super.onSizeChanged(width, height, oldWidth, oldHeight);
            centerX = width / 2.0f;
            centerY = height / 2.0f;
            btnRadius = Math.min(width, height) / 2.0f;
            highlightThickness = btnRadius / 10.0f;
        }

        public void setShowHighlight(boolean highlight) {
            showHighlight = highlight;
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            //Toast.makeText(getContext(), "color is clicked", Toast.LENGTH_SHORT).show();
            if (event.getAction() == MotionEvent.ACTION_DOWN) circleClicked(me);
            //this.gestureDetector.onTouchEvent(event);
            return super.onTouchEvent(event);
        }

        private class mGestureDetector extends GestureDetector.SimpleOnGestureListener {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                //Toast.makeText(getContext(), "color is clicked", Toast.LENGTH_SHORT).show();
                circleClicked(me);
                return super.onSingleTapUp(e);
            }
        }
    }
}
