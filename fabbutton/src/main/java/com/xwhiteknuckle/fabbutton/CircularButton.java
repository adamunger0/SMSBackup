package com.xwhiteknuckle.fabbutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

/**
 * A Google Plus like, circular button for Android.
 * See https://github.com/Alexrs95/CircularButton
 */
public class CircularButton extends ImageView {

    /**
     * The dimension of the shadow is a 15% of the radius of the button
     */
    private float shadowConstant = 0.0f;
    private Paint mButtonPaint;
    private float centerX;
    private float centerY;
    private int btnRadius;
    private int buttonSelectedColor;
    private int buttonUnselectedColor;
    private int shadowColor;
    private int animationDuration = 40;

    private GestureDetectorCompat gestureDetector;

    public CircularButton(Context c) {
        super(c);
        init(c, null);
    }

    public CircularButton(Context c, AttributeSet attrs) {
        super(c, attrs);
        init(c, attrs);
    }

    public CircularButton(Context c, AttributeSet attrs, int defStyle) {
        super(c, attrs, defStyle);
        init(c, attrs);
    }

    private void init(Context c, AttributeSet attrs) {
        setScaleType(ScaleType.CENTER_INSIDE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        mButtonPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mButtonPaint.setStyle(Paint.Style.FILL);

        if (c != null && attrs != null) {
            TypedArray a = c.obtainStyledAttributes(attrs, R.styleable.CircularButton);
            buttonSelectedColor = a.getColor(R.styleable.CircularButton_buttonColorSelected, c.getResources().getColor(R.color.primary_material_dark)); //buttonSelectedColor);
            buttonUnselectedColor = a.getColor(R.styleable.CircularButton_buttonColorUnselected, c.getResources().getColor(R.color.primary_material_light)); // buttonUnselectedColor);
            shadowColor = a.getColor(R.styleable.CircularButton_shadowColor, c.getResources().getColor(R.color.background_material_light)); //shadowColor);
            animationDuration = a.getInt(R.styleable.CircularButton_animationDuration, animationDuration);
            shadowConstant = a.getFloat(R.styleable.CircularButton_shadowConstant, shadowConstant);
            a.recycle();
        }
        setButtonUnselectedColor(buttonUnselectedColor);
        setShadowColor(shadowColor);

        gestureDetector = new GestureDetectorCompat(c, new mGestureDetector());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(centerX, centerY, btnRadius - (btnRadius * shadowConstant), mButtonPaint);
        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        centerX = width / 2;
        centerY = height / 2;
        btnRadius = Math.min(width, height) / 2;

        // the shadow color is settled here because its dimension depends on the radius of the button
        setShadowColor(shadowColor);
    }


    @SuppressWarnings("WeakerAccess")
    public void setButtonSelectedColor(int color) {
        this.buttonSelectedColor = color;
        //mButtonPaint.setColor(buttonSelectedColor);
        //invalidate();
    }

    @SuppressWarnings("WeakerAccess")
    public void setButtonUnselectedColor(int color) {
        this.buttonUnselectedColor = color;
        mButtonPaint.setColor(buttonUnselectedColor);
        invalidate();
    }

    @SuppressWarnings("WeakerAccess")
    public void setShadowColor(int color) {
        this.shadowColor = color;
        mButtonPaint.setShadowLayer(btnRadius * shadowConstant, 0, 0, shadowColor);
        invalidate();
    }

    public int getButtonUnselectedColor() {
        return buttonUnselectedColor;
    }

    public int getButtonSelectedColor() {
        return buttonSelectedColor;
    }

    public int getShadowColor() {
        return shadowColor;
    }

    // TODO: make this animation better...
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    // The scale up ScaleAnimation is unnecessary since the android framework will automatically scale the
    // view back to it's original size as we don't make the scale down permanent. It *looks* like the system
    // uses approx. 40ms animation duration so using that for the way down *appears* consistent
    private void simulateButtonPress() {
        final ImageView thisView = this;

        RectF bounds = new RectF();
        bounds.set((float) this.getLeft(), (float) this.getTop(), (float) this.getRight(), (float) this.getBottom());

        ScaleAnimation scaleDown = new ScaleAnimation(1.0f, 0.95f, 1.0f, 0.95f, this.getWidth() / 2.0f, this.getHeight() / 2.0f);
        scaleDown.setDuration(animationDuration);
        final ScaleAnimation scaleUp = new ScaleAnimation(0.9f, 1.0f, 0.9f, 1.0f, this.getWidth() / 2.0f, this.getHeight() / 2.0f);
        scaleUp.setDuration(40);

        scaleDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mButtonPaint.setColor(buttonSelectedColor);
                thisView.invalidate();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                thisView.startAnimation(scaleUp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        scaleUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                mButtonPaint.setColor(buttonUnselectedColor);
                thisView.invalidate();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        this.startAnimation(scaleDown);
    }

    // To capture "click" and "long press" events - TODO do something with long presses (different animation maybe??)
    private class mGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            simulateButtonPress();
            return super.onSingleTapUp(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }
    }

}
