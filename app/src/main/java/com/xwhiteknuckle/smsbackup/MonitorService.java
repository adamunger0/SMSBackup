package com.xwhiteknuckle.smsbackup;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Telephony.Sms;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;

public class MonitorService extends Service {

    private static final String TAG = MonitorService.class.getSimpleName();
    private static final int NOTIFICATION_ID = 9457;

    private OnUpdateMessages mOnUpdateMessages = null;
    
    @Override
    public void onCreate() {

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mOnUpdateMessages != null) mOnUpdateMessages.cancel(true);
        mOnUpdateMessages = new OnUpdateMessages();
        mOnUpdateMessages.execute();

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        //mOnUpdateMessages.cancel(true);
        super.onDestroy();
    }

    private void stopThisService() {
        stopSelf();
    }

    /**
     * Get the messages from the contacts that we are monitoring and backup these messages to app
     * local storage.
     */
    private class OnUpdateMessages extends AsyncTask<Void, Void, Void> implements DataBaseHelper.ProgressUpdater{
        NotificationCompat.Builder inProgressNotificationBuilder;
        final boolean showNotifications = getShowNotifications();

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {
            //File f = new File(DataBaseHelper.DB_PATH + DataBaseHelper.DB_NAME);
            //Log.d(TAG, f.getAbsolutePath() + (f.exists() ? " The db file already exists" : " The db file doesn't exist yet"));

            try {
                if (showNotifications) {
                    inProgressNotificationBuilder = createNotificationBuilderForBackupInProgress();
                    postNotification(inProgressNotificationBuilder);
                }

                Log.d(TAG, "Beginning messages update...");

                // First, get every text message on this device from the selected contacts
                ArrayList<String> contactNumbers = GroupManager.getGroupContacts(getApplicationContext());
                String[] contactNumbersArray = new String[contactNumbers.size()];
                for (int i = 0; i < contactNumbers.size(); ++i) contactNumbersArray[i] = contactNumbers.get(i);

                // Get all messages from these contacts numbers
                // Full credit for this bit of brilliance comes from:
                // http://stackoverflow.com/questions/7678216/android-matching-phone-number-content-resolver-w-different-formatting
                StringBuilder selectionBuilder = new StringBuilder();
                for (int i = 0; i < contactNumbersArray.length; ++i) {
                    selectionBuilder.append("phone_numbers_equal(" + Sms.ADDRESS + ",'").append(contactNumbersArray[i]).append("')");
                    if (i < contactNumbersArray.length - 1) {
                        selectionBuilder.append(" OR ");
                    }
                }

                // Now, obtain a cursor that points to the messages from our selected contacts
                Cursor mCursor = getContentResolver().query(Sms.CONTENT_URI, null, selectionBuilder.toString(), null, Sms.DEFAULT_SORT_ORDER);

                // Finally, dump the contents of the cursor we just obtained to an app private database
                DataBaseHelper dbManager = new DataBaseHelper(getApplicationContext());
                boolean success = dbManager.copyCursorToDatabase(mCursor);
                String path = dbManager.getReadableDatabase().getPath();
                dbManager.close();

                Log.d(TAG, (success ? "Successfully created db at " : "Unsuccessfully created db at ") + path);

                if (isCancelled() && showNotifications) {
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.cancelAll();
                }
                else if (showNotifications) {
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.cancelAll();
                    postNotification(createNotificationBuilderForCompletedBackup(mCursor.getCount()));
                }

                mCursor.close();

            }
            catch (Exception e) {
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                manager.cancelAll();
                Log.e(TAG, "Unable to successfully complete messages backup.", e);
            }

            return null;
        }
        
        @Override
        protected void onPostExecute(Void aVoid) {
            mOnUpdateMessages = null;
            Intent intent = new Intent();
            intent.setAction(ContactCardAdapter.UPDATED_MESSAGES_ACTION);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            stopThisService();
        }

        @Override
        public void postUpdate(int max, int progress) {
            // TODO: consider filling this in to provide a more complete and accurate notification
            // message to the user whilst backing up his/her messages...
        }
    }

    private void postNotification(NotificationCompat.Builder builder) {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, builder.build());
    }
    
    private NotificationCompat.Builder createNotificationBuilderForCompletedBackup(int numMessages) {
        @SuppressLint("InlinedApi") // Lint complains about Notification.CATEGORY_STATUS but earlier API's just ignore this field...
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
            .setContentTitle(getResources().getString(R.string.app_name))
            .setCategory(Notification.CATEGORY_STATUS)
            .setContentIntent(createPendingIntentForMainActivity())
            .setAutoCancel(true)
            .setOnlyAlertOnce(true) // ?? maybe not ??
            .setLights(Color.MAGENTA, 500, 500)
            .setSmallIcon(R.drawable.ic_stat_ic_launcher)
            .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
            .setTicker(getResources().getString(R.string.backup_complete_message))
            .setContentText(getResources().getString(R.string.backup_complete_message));
            
        // Set this notification to vibrate if the user has set that option in the preferences
        if (getVibrate()) mBuilder.setVibrate(new long[]{0, 250, 250, 250});
            
        // Create a ringtone if the user has set that in the preferences, an empty string corresponds
        // to no ringtone.
        Uri alarmSound = null;
        String s = getRingtoneUriString();
        if (s.length() > 0) alarmSound = Uri.parse(s);
        if (alarmSound != null) mBuilder.setSound(alarmSound);

        // Set what mBuilder has thus far as the publicly visible notification
        Notification notification = mBuilder.build();
        mBuilder.setPublicVersion(notification);
        
        // Now add details that are private - only to be shown to the user
        mBuilder.setNumber(numMessages);
        
        // Finally, return this builder
        return mBuilder;
    }
    
    private NotificationCompat.Builder createNotificationBuilderForBackupInProgress() {
        @SuppressLint("InlinedApi") // Lint complains about Notification.CATEGORY_STATUS but earlier API's just ignore this field...
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
            .setContentTitle(getResources().getString(R.string.app_name))
            .setCategory(Notification.CATEGORY_STATUS)
            .setAutoCancel(true)
            .setContentIntent(createPendingIntentForMainActivity())
            .setLights(Color.YELLOW, 500, 500)
            .setSmallIcon(android.R.drawable.ic_popup_sync)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setUsesChronometer(true)
            .setProgress(0, 0, true)
            .setTicker(getResources().getString(R.string.in_progress_ticker_message))
            .setContentText(getResources().getString(R.string.in_progress_message));
            
        Notification notification = mBuilder.build();
        mBuilder.setPublicVersion(notification);
        
        return mBuilder;
    }
    
    private PendingIntent createPendingIntentForMainActivity() {
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);
        // The stack builder object will contain an artificial back stack for the started Activity.
        // This ensures that navigating backward from the Activity leads out of your application
        // to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private boolean getShowNotifications() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return prefs.getBoolean("notifications_new_message", true);
    }

    private boolean getVibrate() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return prefs.getBoolean("notifications_new_message_vibrate", true);
    }

    private String getRingtoneUriString() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return prefs.getString("notifications_new_message_ringtone", "");
    }
}
