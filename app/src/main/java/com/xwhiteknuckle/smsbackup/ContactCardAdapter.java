package com.xwhiteknuckle.smsbackup;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContactCardAdapter extends RecyclerView.Adapter<ViewHolder> implements RecyclerViewItemClickInterface {
    public static final String TAG = ContactCardAdapter.class.getSimpleName();
    public static final String UPDATED_MESSAGES_ACTION = "com.xwhiteknuckle.smsbackup.updated_messages";

    private DataBaseHelper mDataBaseHelper;
    private Cursor mCursor;

    private boolean showContacts = true;
    private String[] contactNames;

    private String profileUri;

    public ContactCardAdapter(Context context) {

        mDataBaseHelper = new DataBaseHelper(context);
        profileUri = GroupManager.getUserProfilePhotoUri(context);

        // Now register a receiver so that we can be notified when our underlying dataset changes
        MessagesCursorChangedReceiver receiver = new MessagesCursorChangedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPDATED_MESSAGES_ACTION);
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);

        updateCursor();
    }

    /**
     * Get a cursor that contains the data for either contacts or all messages from a single contact
     */
    private void updateCursor() {
        if (mCursor != null) mCursor.close();
        if (showContacts) {
            Log.d(TAG, "Updating cursor to contacts list");
            mCursor = mDataBaseHelper.contactQuery(new String[]{DataBaseConstants.BODY,
                            DataBaseConstants.ADDRESS,
                            DataBaseConstants.DISPLAY_NAME_PRIMARY,
                            DataBaseConstants.PHOTO_URI,
                            DataBaseConstants.DATE},
                    DataBaseConstants.DATE + " DESC");
        }
        else {
            Log.d(TAG, "Updating cursor to messages list");
            mCursor = mDataBaseHelper.messageQuery(new String[]{DataBaseConstants.BODY,
                            DataBaseConstants.ADDRESS,
                            DataBaseConstants.DISPLAY_NAME_PRIMARY,
                            DataBaseConstants.PHOTO_URI,
                            DataBaseConstants.PERSON,
                            DataBaseConstants.DATE},
                    contactNames,
                    DataBaseConstants.DATE + " DESC");
        }
        notifyDataSetChanged();
    }

    public boolean onBackPressed() {
        if (!showContacts) {
            showContacts = true;
            contactNames = null;
            updateCursor();
            return true;
        }

        return false;
    }

    /**
     * Not to be confused with the different type of ViewHolders. This is just simply to determine
     * if this position is a padding view or a data view. The only position where a padding view is
     * required is index 0.
     * @param position the position in the list of displayed items for which we are interested
     * @return either ContactCardAdapter.VIEW_TYPE_PADDING or ContactCardAdapter.VIEW_TYPE_ITEM
     */
    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ViewHolder.PADDING_VIEW_HOLDER_ID;
            default:
                if (showContacts) return ViewHolder.CONTACT_VIEW_HOLDER_ID;
                else {
                    if (mCursor != null && position > 0) {
                        mCursor.moveToPosition(position - 1);
                        if (mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.PERSON)) == null)
                            return ViewHolder.RIGHT_MESSAGE_VIEW_HOLDER_ID;
                        else
                            return ViewHolder.LEFT_MESSAGE_VIEW_HOLDER_ID;
                    }
                    return ViewHolder.PADDING_VIEW_HOLDER_ID;
                }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case ViewHolder.LEFT_MESSAGE_VIEW_HOLDER_ID:
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.backed_up_message_left_card, viewGroup, false);
                return new ViewHolder(viewGroup, v, this);
            case ViewHolder.RIGHT_MESSAGE_VIEW_HOLDER_ID:
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.backed_up_message_right_card, viewGroup, false);
                return new ViewHolder(viewGroup, v1, this);
            case ViewHolder.CONTACT_VIEW_HOLDER_ID:
                View v2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.backed_up_contact_card, viewGroup, false);
                return new ViewHolder(viewGroup, v2, this);
            default:
                View v3 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.padding_view, viewGroup, false);
                return new ViewHolder(viewGroup, v3, this);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        if (mCursor != null && position > 0) {
            mCursor.moveToPosition(position - 1);
            if (showContacts) {
                vh.setViewHolderAsContactView(
                        mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.BODY)),
                        mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.ADDRESS)),
                        mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.DISPLAY_NAME_PRIMARY)),
                        mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.PHOTO_URI)),
                        mCursor.getLong(mCursor.getColumnIndex(DataBaseConstants.DATE))
                );
            }
            else {
                // If PERSON is null, then this message is a sent message
                if (mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.PERSON)) == null) {
                    vh.setViewHolderAsRightMessageView(
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.BODY)),
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.ADDRESS)),
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.DISPLAY_NAME_PRIMARY)),
                            profileUri,
                            mCursor.getLong(mCursor.getColumnIndex(DataBaseConstants.DATE))
                    );
                }
                else {
                    vh.setViewHolderAsLeftMessageView(
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.BODY)),
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.ADDRESS)),
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.DISPLAY_NAME_PRIMARY)),
                            mCursor.getString(mCursor.getColumnIndex(DataBaseConstants.PHOTO_URI)),
                            mCursor.getLong(mCursor.getColumnIndex(DataBaseConstants.DATE))
                    );
                }
            }
        }
        else vh.setViewHolderAsPaddingView();
    }

    /**
     * Return the number of rows in our input data cursor plus one for the padding row at the top,
     * underneath the Toolbar.
     * @return the number of rows in the contacts cursor + 1, or 0 if the cursor is null
     */
    @Override
    public int getItemCount() {
        if (mCursor != null) return mCursor.getCount() + 1;
        return 0;
    }

    /**
     * User's should call this to ensure that the cursor and database get properly closed
     */
    public void onDestroy() {
        if (mCursor != null) mCursor.close();
        if (mDataBaseHelper != null) mDataBaseHelper.close();
    }

    /**
     * Will process clicks from the views in this RecyclerView's list of views.
     * @param v the view that was clicked
     * @param id one of MESSAGE_VIEW_HOLDER_ID, ITEM_VIEW_HOLDER_ID or PADDING_VIEW_HOLDER_ID
     */
    @Override
    public void onItemClick(View v, int id) {
        /*if (id == ViewHolder.MESSAGE_VIEW_HOLDER_ID)
            Toast.makeText(mContext, "MESSAGE_VIEW " + ((TextView) v.findViewById(R.id.left_message_card_name)).getText(), Toast.LENGTH_SHORT).show();
        else if (id == ViewHolder.CONTACT_VIEW_HOLDER_ID)
            Toast.makeText(mContext, "CONTACT_VIEW " + ((TextView) v.findViewById(R.id.contact_card_name)).getText(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "PADDING_VIEW ", Toast.LENGTH_SHORT).show();*/

        showContacts = !showContacts;

        switch (id) {
            case ViewHolder.LEFT_MESSAGE_VIEW_HOLDER_ID:
                contactNames = null;
                updateCursor();
                break;
            case ViewHolder.RIGHT_MESSAGE_VIEW_HOLDER_ID:
                contactNames = null;
                updateCursor();
                break;
            case ViewHolder.CONTACT_VIEW_HOLDER_ID:
                contactNames = new String[1];
                contactNames[0] = (String) ((TextView) v.findViewById(R.id.contact_card_name)).getText();
                updateCursor();
                break;
            default:
                // do nothing for padding view clicks
        }
    }

    private class MessagesCursorChangedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context p1, Intent p2) {
            String action = p2.getAction();
            if (action.equals(UPDATED_MESSAGES_ACTION)) {
                Log.d(TAG, "Underlying dataset has changed");
                updateCursor();
                notifyDataSetChanged();
            }
            else if (action.equals(MainActivity.CHANGE_COLOR_THEME_ACTION)) {
                notifyDataSetChanged(); // TODO: surely there is a better way...maybe a broadcast receiver inside each viewholder??
            }
        }
    }
}
