package com.xwhiteknuckle.smsbackup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String TAG = DataBaseHelper.class.getSimpleName();

    // Constants for db creation
    public static final int DB_VERSION = 1;
    //public static String DB_PATH;
    public static final String DB_NAME = "MessageBackup";

    private Context mContext;
    public ProgressUpdater updater;

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
        //DB_PATH = context.getFilesDir().getAbsolutePath() + File.separator;
    }

    public void copyDataBaseToExternalFile(Uri outFileUri) throws IOException {
        // Open local db as the input stream
        File f = new File(this.getDatabaseName());
        //Log.d(TAG, f.getAbsolutePath() + (f.exists() ? " file exists" : " file doesn't exist"));
        FileInputStream myInput = new FileInputStream(f);

        // Open the empty db as the output stream
        OutputStream myOutput = mContext.getContentResolver().openOutputStream(outFileUri);

        // Transfer bytes from the input file to the output file
        byte[] buffer = new byte[1024];
        int length;
        int totalBytes = 0;
        while ((length = myInput.read(buffer)) > 0) {
            totalBytes += 1024;
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

        Log.d(TAG, "Duplicated " + String.valueOf(totalBytes) + " bytes to " + outFileUri.getPath());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DB_NAME + " ("
                + DataBaseConstants._ID + " INTEGER PRIMARY KEY, " // long
                + DataBaseConstants._COUNT + " INTEGER, "
                + DataBaseConstants.ADDRESS + " TEXT, "
                + DataBaseConstants.BODY + " TEXT, "
                + DataBaseConstants.CREATOR + " TEXT, "
                + DataBaseConstants.DATE + " INTEGER, " // long
                + DataBaseConstants.DATE_SENT + " INTEGER, " // long
                + DataBaseConstants.ERROR_CODE + " INTEGER, "
                + DataBaseConstants.LOCKED + " INTEGER, " // boolean
                + DataBaseConstants.PERSON + " INTEGER, " // (reference to item in content://contacts/people)
                + DataBaseConstants.PROTOCOL + " INTEGER, "
                + DataBaseConstants.READ + " INTEGER, " // boolean
                + DataBaseConstants.REPLY_PATH_PRESENT + " BOOLEAN, "
                + DataBaseConstants.SEEN + " INTEGER, "
                + DataBaseConstants.SERVICE_CENTER + " TEXT, "
                + DataBaseConstants.STATUS + " INTEGER, "
                + DataBaseConstants.SUB_ID + " INTEGER, "
                + DataBaseConstants.SUBJECT + " TEXT, "
                + DataBaseConstants.THREAD_ID + " INTEGER, "
                + DataBaseConstants.TYPE + " INTEGER,"
                + DataBaseConstants.DISPLAY_NAME_PRIMARY + " TEXT,"
                + DataBaseConstants.PHOTO_URI + " TEXT"
                + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Logs that the database is being upgraded
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");

        // Kills the table and existing data
        db.execSQL("DROP TABLE IF EXISTS " + DB_NAME);

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private String[] getContactNameAndThumbnailFromNumber(String number) {
        Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor contact = mContext.getContentResolver().query(uri, new String[]{ PhoneLookup.DISPLAY_NAME, PhoneLookup.PHOTO_URI }, null, null, null);

        // TODO: maybe add IS_PRIMARY to search and get only the name and photo for that contact...
        String[] rval = new String[2];
        if (contact != null && contact.moveToFirst()) {
            rval[0] = contact.getString(contact.getColumnIndex(PhoneLookup.DISPLAY_NAME));
            rval[1] = contact.getString(contact.getColumnIndex(PhoneLookup.PHOTO_URI));
        }

        contact.close();

        return rval;
    }

    /**
     * Makes a copy of the input Cursor which is presumably from the Telephony.Sms uri. If it isn't,
     * the original db of messages (if one existed) will have been deleted and then the call to
     * DatabaseUtils.cursorRowToContent(Cursor, ContentValues) will likely throw an
     * android.database.sqlite.SQLiteException for each row, but the app will keep running without
     * having actually copied anything at all.
     * @param dbToCopy a cursor that points to Telephony.Sms or one of it's descendants
     * @return false if nothing was copied, or true if at least one row was copied to app local
     * storage (even if nothing was copied due to the cursor pointing to an incorrect data source).
     */
    public boolean copyCursorToDatabase(Cursor dbToCopy) {
        // If the input Cursor is empty then there's nothing to do
        if (dbToCopy.moveToFirst()) {
            // Get a copy of the editable database that this class represents
            SQLiteDatabase db = this.getWritableDatabase();
            // Kills the table and existing data
            db.execSQL("DROP TABLE IF EXISTS " + DB_NAME);
            // Recreates the database
            onCreate(db);

            // Variables for tracking the progress of the db creation for the benefit of any progress listener that may be defined
            int numRows = dbToCopy.getCount();
            int numRowsCompleted = 0;

            // Since we're here the input Cursor must have at least one row, so start copying
            do {
                // Use a content values to define a single row in the new database - add 1 for the name column
                ContentValues values = new ContentValues(dbToCopy.getColumnCount() + 1);
                // Use DatabaseUtils to copy a row from the input cursor to this database
                DatabaseUtils.cursorRowToContentValues(dbToCopy, values);
                String[] contactInfo = getContactNameAndThumbnailFromNumber(dbToCopy.getString(dbToCopy.getColumnIndex(DataBaseConstants.ADDRESS)));
                values.put(DataBaseConstants.DISPLAY_NAME_PRIMARY, contactInfo[0]);
                values.put(DataBaseConstants.PHOTO_URI, contactInfo[1]);

                // Insert the ContentValues into our database
                db.insert(DB_NAME, null, values);

                // Update a progress listener if the caller has bothered to specify one
                if (updater != null) updater.postUpdate(numRows, ++numRowsCompleted);
            } while (dbToCopy.moveToNext());

            String path = db.getPath();
            db.close();

            File f = new File(path);
            Log.d(TAG, (f.exists() ? "Created " + String.valueOf(f.length()) + " bytes at " + path : "Unable to create app local database"));

            return true;
        }

        return false;
    }

    /**
     * Returns a cursor to the current db in the apps local storage. If the db doesn't exists or is
     * unable to be opened, this returns null.
     * @return a cursor over the local db, or null
     */
    public Cursor query(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        SQLiteDatabase db = null;

        db = this.getReadableDatabase();// SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.OPEN_READONLY);

        return db != null ? db.query(DB_NAME, columns, selection, selectionArgs, groupBy, having, orderBy) : null;
    }

    public  Cursor contactQuery(String[] columns, String orderBy) {
        String[] newColumns = new String[columns.length + 1];
        for (int i = 0; columns != null && i < columns.length; ++i) newColumns[i] = columns[i];
        newColumns[newColumns.length - 1] = "MAX(" + DataBaseConstants.DATE + ")";
        return query(columns, null, null, DataBaseConstants.DISPLAY_NAME_PRIMARY, null, orderBy);
    }

    public  Cursor messageQuery(String[] columns, String[] names, String orderBy) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; names != null && i < names.length; ++i) {
            sb.append(DataBaseConstants.DISPLAY_NAME_PRIMARY + "=?");
            if (i < names.length - 1) sb.append(", ");
        }
        return query(columns, sb.toString(), names, null, null, orderBy); // TODO: columns shouldn't be null...
    }

    public static class DuplicateMessagesDb extends AsyncTask<Uri, Void, Void> {
        Context context;
        public DuplicateMessagesDb(Context c) {
            context = c;
        }
        @Override
        protected Void doInBackground(Uri... uri) {
            DataBaseHelper helper = new DataBaseHelper(context);
            try {
                helper.copyDataBaseToExternalFile(uri[0]);
            }
            catch (IOException e) {
                Log.e(TAG, "Unable to copy internal database to external storage", e);
            }
            return null;
        }
    }

    public static interface ProgressUpdater {
        public void postUpdate(int max, int progress);
    }
}