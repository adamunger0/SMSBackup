package com.xwhiteknuckle.smsbackup;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.xwhiteknuckle.animatedtoolbar.AnimatedToolbar;

import java.util.Set;
import java.util.TreeSet;

public class SettingsActivity extends ActionBarActivity
        implements GoogleApiClient.ConnectionCallbacks,
                   GoogleApiClient.OnConnectionFailedListener {
    
    public static final int ALARM_SERVICE_REQUEST_CODE = 7742;
    public static final String GOOGLE_DRIVE_SELECTED_ACTION = "com.xwhiteknuckle.smsbackup.drive_selected";
    public static final String GOOGLE_DRIVE_NOT_SELECTED_ACTION = "com.xwhiteknuckle.smsbackup.drive_not_selected";

    /*
     * Request code for google drive connection
     */
    public static final int RESOLVE_CONNECTION_REQUEST_CODE = 2123;

    private GoogleApiClient mGoogleApiClient;
    private SetupGoogleDriveReceiver receiver;
    private boolean isSavingToDrive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ThemeSwitcher.onActivityCreateSetTheme(this);
        
        setContentView(R.layout.activity_settings);
    
        PreferenceManager.setDefaultValues(this, R.xml.pref_all, false);
        
        setupToolbar();

        isSavingToDrive = isSavingToGoogleDrive();
        //if (isSavingToDrive)
            //setupGoogleApiClient();
    }

    private void setupToolbar() {
        AnimatedToolbar toolbar = (AnimatedToolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    private void setupGoogleApiClient() {
        if (mGoogleApiClient != null && (mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting())) {
            //Toast.makeText(this, "drive is connecting or is connected", Toast.LENGTH_SHORT).show();
        }
        else {
            //Toast.makeText(this, "creating drive api client", Toast.LENGTH_SHORT).show();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        }
    }

    private void restartActivity() {
        recreate();
    }
    
    private boolean isSavingToGoogleDrive() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> locations = prefs.getStringSet("storage_location", new TreeSet<String>());
        for (String location : locations) 
            if (location.equals("Google Drive"))
                return true;
        return false;
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        //if (isSavingToDrive)
            //mGoogleApiClient.connect();
            
        receiver = new SetupGoogleDriveReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(GOOGLE_DRIVE_SELECTED_ACTION);
        filter.addAction(GOOGLE_DRIVE_NOT_SELECTED_ACTION);
        filter.addAction(MainActivity.CHANGE_COLOR_THEME_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }
    
    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && (mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting()))
            mGoogleApiClient.disconnect();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Toast.makeText(this, "connected to drive", Toast.LENGTH_SHORT).show();
        /*
        IntentSender intentSender = Drive.DriveApi
            .newOpenFileActivityBuilder()
            .setMimeType(new String[] {MIMETYPE})
            .build(mGoogleApiClient);
        try {
            startIntentSenderForResult(intentSender, OPEN_FILE_REQUEST_CODE, null, 0, 0, 0);
        }
        catch (IntentSender.SendIntentException e) {
            Log.w(TAG, "Unable to send intent", e);
        }
        */
    }

    @Override
    public void onConnectionSuspended(int p1) {
        // TODO: Implement this method
    }

    /*
     * Google drive callback - will be called when the user hasn't yet authorized this app for their
     * drive and for other failures.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, RESOLVE_CONNECTION_REQUEST_CODE);
            }
            catch (IntentSender.SendIntentException e) {
                // Unable to resolve, message user appropriately
            }
        }
        else {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case RESOLVE_CONNECTION_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mGoogleApiClient.connect();
                }
                else if (resultCode == RESULT_CANCELED) {
                    // TODO something
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                        // Add all of this activity's parents to the back stack
                        .addNextIntentWithParentStack(upIntent)
                        // Navigate up to the closest parent
                        .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private static boolean isSimplePreferences(Context context) {
        return !isXLargeTablet(context);
    }

    /**
     * A preference value change listener that updates the preference's summary to reflect its new value.
     */
    private static final Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            if (preference instanceof MultiSelectListPreference) {
                Set<String> values = (Set<String>) value;
                StringBuilder sb = new StringBuilder();
                int commaCount = 0;
                boolean gDrive = false;
                for (String v : values) {
                    sb.append(v);
                    if (v.equals("Google Drive")) gDrive = true;
                    if (commaCount < values.size() - 1) sb.append(", ");
                    ++commaCount;
                }
                preference.setSummary(sb.toString());
                Intent googleDriveIntent = new Intent();
                if (gDrive) googleDriveIntent.setAction(GOOGLE_DRIVE_SELECTED_ACTION);
                else googleDriveIntent = new Intent().setAction(GOOGLE_DRIVE_NOT_SELECTED_ACTION);
                LocalBroadcastManager manager = LocalBroadcastManager.getInstance(preference.getContext());
                manager.sendBroadcast(googleDriveIntent);
            }
            // TODO this needs to be more robust so user can select any sound they want....
            else if (preference instanceof RingtonePreference) {
                String stringValue = value.toString();
                // For ringtone preferences, look up the correct display value using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(R.string.pref_ringtone_silent);
                } 
                else {
                    Ringtone ringtone = RingtoneManager.getRingtone(preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error
                        preference.setSummary(null);
                    }
                    else {
                        // Set the summary to reflect the new ringtone display name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            }
            else if (preference instanceof ListPreference && preference.getKey().equals("sync_frequency")) {
                // Turn the input selection into a String for further processing
                String stringValue = value.toString();
                long number = Long.valueOf(stringValue);
                
                // First, create a system alarm that will call the MonitorService at the user specified interval if user hasn't chosen 0
                AlarmManager am = (AlarmManager) preference.getContext().getSystemService(ALARM_SERVICE);
                if (am != null) {
                    Intent intent = new Intent(preference.getContext().getApplicationContext(), MonitorService.class);
                    PendingIntent pIntent = PendingIntent.getService(preference.getContext().getApplicationContext(), ALARM_SERVICE_REQUEST_CODE, intent, 0);
                    am.cancel(pIntent);
                    if (number > 0) {
                        long interval = Long.valueOf(stringValue) * 1000l;
                        long first =  System.currentTimeMillis() + interval;
                        am.setRepeating(AlarmManager.RTC, first, interval, pIntent);
                    }
                }
                
                // Next, get the human-friendly version to update the pref display with
                String[] values = preference.getContext().getResources().getStringArray(R.array.pref_sync_frequency_values);
                String[] titles = preference.getContext().getResources().getStringArray(R.array.pref_sync_frequency_titles);
                boolean found = false;
                for (int i = 0; i < values.length; ++i) {
                    if (stringValue.equals(values[i])) {
                        preference.setSummary(titles[i]);
                        found = true;
                        break;
                    }
                }
                if (!found) preference.setSummary(stringValue);
            }
            else if (preference instanceof ListPreference && preference.getKey().equals("pref_theme")) {
                String stringValue = value.toString();
                preference.setSummary(stringValue);
                Intent themeChangeIntent = new Intent().setAction(MainActivity.CHANGE_COLOR_THEME_ACTION);
                LocalBroadcastManager manager = LocalBroadcastManager.getInstance(preference.getContext());
                manager.sendBroadcast(themeChangeIntent);
            }
            else {
                String stringValue = value.toString();
                // For all other preferences, set the summary to the value's simple string representation
                preference.setSummary(stringValue);
            }
            
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        if (preference instanceof MultiSelectListPreference)
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getStringSet(preference.getKey(), new TreeSet<String>()));
        else
            // Trigger the listener immediately with the preference's current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), ""));
    }
    
    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    public static class AllPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_all);
            
            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
            //bindPreferenceSummaryToValue(findPreference("storage_location"));
            bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
            bindPreferenceSummaryToValue(findPreference("pref_theme"));
        }
    }
    
    private class SetupGoogleDriveReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context p1, Intent p2) {
            String action = p2.getAction();
            //Toast.makeText(p1, action, Toast.LENGTH_SHORT).show();
            if (action.equals(GOOGLE_DRIVE_SELECTED_ACTION)) {
                isSavingToDrive = true;
                setupGoogleApiClient();
            }
            else if (action.equals(GOOGLE_DRIVE_NOT_SELECTED_ACTION)) {
                isSavingToDrive = false;
                if (mGoogleApiClient != null) mGoogleApiClient.disconnect();
            }
            else if (action.equals(MainActivity.CHANGE_COLOR_THEME_ACTION)) {
                restartActivity();
            }
        }
    }
}
