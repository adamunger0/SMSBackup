package com.xwhiteknuckle.smsbackup;

public class DataBaseConstants {
    public static final String _ID = "_id";
    public static final String _COUNT = "_count";
    public static final String ADDRESS = "address";
    public static final String BODY = "body";
    public static final String CREATOR = "creator";
    public static final String DATE = "date"; // date the message was received on this device
    public static final String DATE_SENT = "date_sent"; // date that the sender sent the message - 0 if the message was sent from this device
    public static final String ERROR_CODE = "error_code";
    public static final String LOCKED = "locked";
    public static final String PERSON = "person"; // contact id if message was recieved from contact, 0 otherwise (if it was sent)
    public static final String PROTOCOL = "protocol";
    public static final String READ = "read";
    public static final String REPLY_PATH_PRESENT = "reply_path_present";
    public static final String SEEN = "seen";
    public static final String SERVICE_CENTER = "service_center";
    public static final String STATUS = "status";
    public static final String SUBJECT = "subject";
    public static final String THREAD_ID = "thread_id";
    public static final String TYPE = "type";
    public static final String SUB_ID = "sub_id";
    public static final String DISPLAY_NAME_PRIMARY = "display_name_primary";
    public static final String PHOTO_URI = "photo_uri";
}
