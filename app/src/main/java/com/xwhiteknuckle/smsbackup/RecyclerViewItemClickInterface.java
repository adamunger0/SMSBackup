package com.xwhiteknuckle.smsbackup;


import android.view.View;

public interface RecyclerViewItemClickInterface {
    public void onItemClick(View v, int id);
}