package com.xwhiteknuckle.smsbackup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Groups;
import android.util.Log;

import java.util.ArrayList;


public class GroupManager {
    
    protected static final String TAG = GroupManager.class.getSimpleName();
    
    public static final String GROUP_TITLE = "Message Backup";
	public static final String GROUP_NOTES = "Backup messages to/from selected contacts";
    // Use com.google so that this group shows up in stock contacts app and is automatically backed-up on Google's servers
    public static final String GROUP_GOOGLE_ACCOUNT_TYPE = "com.google";
	
	public static boolean addGroup(Context context) {
		ArrayList<ContentProviderOperation> ops = new ArrayList<>(1);
        ops.add(ContentProviderOperation.newInsert(Groups.CONTENT_URI)
                .withValue(Groups.TITLE, GROUP_TITLE)
                .withValue(Groups.NOTES, GROUP_NOTES)
                .withValue(Groups.ACCOUNT_TYPE, GROUP_GOOGLE_ACCOUNT_TYPE)
                .withValue(Groups.ACCOUNT_NAME, getCurrentUserEmail(context))
                .withValue(Groups.GROUP_VISIBLE, 1)
                .build());
        
		try {
			ContentProviderResult[] results = context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            // TODO do something with the results
		}
		catch (Exception e) {
			e.printStackTrace();
            return false;
		}
        return true;
	}
   
    public static String getCurrentUserEmail(Context context) {
        final String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.IS_PRIMARY,
                ContactsContract.CommonDataKinds.Photo.PHOTO_URI,
                ContactsContract.Contacts.Data.MIMETYPE
        };
        final ContentResolver content = context.getContentResolver();
        final Cursor cursor = content.query(
                // Retrieves data rows for the device user's 'profile' contact
                Uri.withAppendedPath(
                        ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
                PROJECTION,

                // Selects only email addresses or names
                ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                        + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                        + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                        + ContactsContract.Contacts.Data.MIMETYPE + "=?",
                new String[]{
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                },

                // Show primary rows first. Note that there won't be a primary email address if the
                // user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC"
        );

        String mimeType;
        String rval = "";
        if (cursor.moveToFirst()) {
            do {
                mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE));
                if (mimeType.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                    rval = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                    if (rval.contains("@gmail.com")) {
                        break;
                    }
                    rval = "";
                }
            } while (cursor.moveToNext());
        }

        Log.d(TAG, "Profile email address: '" + rval + "'");
        cursor.close();
        return rval;

        /*String mime_type;
        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                mime_type = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE));
                if (mime_type.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE))
                    user_profile.addPossibleEmail(cursor.getString(ProfileQuery.EMAIL),
                            cursor.getInt(ProfileQuery.IS_PRIMARY_EMAIL) > 0);
                else if (mime_type.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE))
                    user_profile.addPossibleName(cursor.getString(ProfileQuery.GIVEN_NAME) + " " + cursor.getString(ProfileQuery.FAMILY_NAME));
                else if (mime_type.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE))
                    user_profile.addPossiblePhoneNumber(cursor.getString(ProfileQuery.PHONE_NUMBER),
                            cursor.getInt(ProfileQuery.IS_PRIMARY_PHONE_NUMBER) > 0);
                else if (mime_type.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE))
                    user_profile.addPossiblePhoto(Uri.parse(cursor.getString(ProfileQuery.PHOTO)));
            }
        }*/



        /*AccountManager manager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        String gmail = null;

        for(Account account : list) {
            if(account.type.equalsIgnoreCase("com.google")) {
                gmail = account.name;
                break;
            }
        }
        //Log.d(TAG, gmail);
        return gmail;*/
    }

    // https://gist.github.com/imminent/4061516
    // TODO: all this extra fluff is here as inspiration only...delete this shit when it's not useful anymore...
    public static String getUserProfilePhotoUri(Context context) {
        final String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.IS_PRIMARY,
                ContactsContract.CommonDataKinds.Photo.PHOTO_URI,
                ContactsContract.Contacts.Data.MIMETYPE
        };
        final ContentResolver content = context.getContentResolver();
        final Cursor cursor = content.query(
                // Retrieves data rows for the device user's 'profile' contact
                Uri.withAppendedPath(
                        ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
                PROJECTION,

                // Selects only email addresses or names
                ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                        + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                        + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                        + ContactsContract.Contacts.Data.MIMETYPE + "=?",
                new String[]{
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                },

                // Show primary rows first. Note that there won't be a primary email address if the
                // user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC"
        );

        String mimeType;
        String rval = "";
        if (cursor.moveToFirst()) {
            rval = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_URI));
            /*do {
                mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE));
                if (mimeType.equals(ContactsContract.CommonDataKinds.Photo.PHOTO_URI)) {
                    rval = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_URI));
                    break;
                }
            } while (cursor.moveToNext());*/
        }

        Log.d(TAG, "Profile photo uri: '" + rval + "'");
        cursor.close();
        return rval;

        /*String mime_type;
        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                mime_type = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE));
                if (mime_type.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE))
                    user_profile.addPossibleEmail(cursor.getString(ProfileQuery.EMAIL),
                            cursor.getInt(ProfileQuery.IS_PRIMARY_EMAIL) > 0);
                else if (mime_type.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE))
                    user_profile.addPossibleName(cursor.getString(ProfileQuery.GIVEN_NAME) + " " + cursor.getString(ProfileQuery.FAMILY_NAME));
                else if (mime_type.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE))
                    user_profile.addPossiblePhoneNumber(cursor.getString(ProfileQuery.PHONE_NUMBER),
                            cursor.getInt(ProfileQuery.IS_PRIMARY_PHONE_NUMBER) > 0);
                else if (mime_type.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE))
                    user_profile.addPossiblePhoto(Uri.parse(cursor.getString(ProfileQuery.PHOTO)));
            }
        }*/
    }
    
    public static int removeGroup(Context context) {
        return context.getContentResolver().delete(Groups.CONTENT_URI, Groups.TITLE + " = ?", new String[] {GROUP_TITLE});
    }

	public static ArrayList<String> getGroupContacts(Context context) {
        ArrayList<String> groupContactNumbers = new ArrayList<>();

        // First, get the group id for this apps group
        String[] projection = { Groups._ID, Groups.TITLE };
        String selection = ContactsContract.Groups.TITLE + " = ?";
        String[] selectionArgs = { GROUP_TITLE };
        Cursor c = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, projection, selection, selectionArgs, null);

        String groupId = "";
        if (c != null && c.moveToFirst()) {
            do {
                if (c.getString(c.getColumnIndex(ContactsContract.Groups.TITLE)).equals(GROUP_TITLE)) {
                    groupId = c.getString(c.getColumnIndex(Groups._ID));
                    break;
                }
            } while (c.moveToNext());
        }
        if (c != null) c.close();

        // Next, get all the contacts that belong to the groupId that we just obtained
        String[] groupProjection = { ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.GroupMembership.CONTACT_ID };
        String groupSelection =
                ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID + "= ?" + " AND "
                + ContactsContract.CommonDataKinds.GroupMembership.MIMETYPE + "='"
                + ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE + "'";
        String[] groupSelectionArgs = { groupId };
        Cursor groupCursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, groupProjection, groupSelection, groupSelectionArgs, null);

        // Last, iterate through the group cursor and get all of the phone numbers for the contacts we just found in our group
        if (groupCursor != null && groupCursor.moveToFirst()) {
            do {
                int cursorNameIndex = groupCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = groupCursor.getString(cursorNameIndex);

                long contactId = groupCursor.getLong(groupCursor.getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership.CONTACT_ID));

                // Now get all of the contacts phone numbers
                String[] numberProjection = { ContactsContract.CommonDataKinds.Phone.NUMBER };
                String numberSelection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId;
                Cursor numberCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, numberProjection, numberSelection, null, null);
                if (numberCursor != null && numberCursor.moveToFirst()) {
                    int numberColumnIndex = numberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                    do {
                        String phoneNumber = numberCursor.getString(numberColumnIndex);
                        if (phoneNumber != null && phoneNumber.length() > 0) groupContactNumbers.add(phoneNumber);
                    } while (numberCursor.moveToNext());

                    numberCursor.close();
                }
                else {
                    Log.d(TAG, "Contact '" + name + "' has no associated phone numbers...");
                    if (numberCursor != null) numberCursor.close();
                }
            } while (groupCursor.moveToNext());

            groupCursor.close();
        }
        else {
            Log.d(TAG, "The user hasn't added any contacts to " + GROUP_TITLE + ". Does the group exist?");
            // TODO: maybe check that the group exists....no duplicates....other stuff that might prevent the user from adding a contact to this group
            if (groupCursor != null) groupCursor.close();
        }

        return groupContactNumbers;
    }
    
    public static boolean checkGroupAvailability(Context context) {
        String[] projection = {Groups.TITLE};
        String selection = Groups.TITLE + " = ?";
        String[] selectionArgs = {GROUP_TITLE};
        Cursor c = context.getContentResolver().query(Groups.CONTENT_URI, projection, selection, selectionArgs, null);
        
        if (c.getCount() > 1) {
            fixGroupDuplicate(context);
            c.close();
            c = context.getContentResolver().query(Groups.CONTENT_URI, projection, selection, selectionArgs, null);
            if (c.getCount() > 1) Log.e(TAG, "For some reason this device has multiple contact groups with the title '" + GROUP_TITLE + "'");
        }
        
        c.moveToFirst();

        boolean rval;
        rval = c.getCount() > 0 && GROUP_TITLE.equals(c.getString(c.getColumnIndex(Groups.TITLE)));
        c.close();

        return rval;
    }
    
    public static void fixGroupDuplicate(Context context) throws IllegalStateException {
        removeGroup(context);
        // TODO figure out how to remove a duplicate group...
    }
    
    public static boolean ensureGroupAvailable(Context context) {
        return checkGroupAvailability(context) || addGroup(context);
    }

    protected static class SetupGroupTask extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... p1) {
            return GroupManager.ensureGroupAvailable(p1[0]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result.equals(Boolean.TRUE)) {
                // TODO uh-oh....can't create group; nothing to monitor and user can't give us anything to monitor....
            }
        }
    }
}
