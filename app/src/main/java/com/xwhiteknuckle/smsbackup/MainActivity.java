package com.xwhiteknuckle.smsbackup;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xwhiteknuckle.animatedtoolbar.AnimatedToolbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


public class MainActivity extends ActionBarActivity {

    private final String TAG = this.getClass().getSimpleName();

    /*
     * Request code for the settings activity
     */
    public static final int SETTINGS_REQUEST_CODE = 1951;

    /*
     * Request code for google drive open file intent
     */
    public static final int OPEN_FILE_REQUEST_CODE = 9512;

    /*
     * Request code for google drive create file intent
     */
    public static final int CREATE_FILE_REQUEST_CODE = 3575;

    /*
     * Define the mime type that this program will use for it's sqlite3 files
     */
    public static final String MIMETYPE = "android.vnd.cursor.item/octet-stream";

    public static final String DEFAULT_BACKUP_TITLE = "MessageBackup";

    public static final String CHANGE_COLOR_THEME_ACTION = "com.xwhiteknuckle.smsbackup.change_theme";

    /*
     * Define the file type that this program will use for it's sqlite3 files
     */
    public static final String FILE_EXTENSION = ".sqlite";

    ContactCardAdapter mContactCardAdapter;
    RecyclerView recyclerView;
    private AnimatedToolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Must set the Activity theme before setContentView is called
        ThemeSwitcher.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.messages_recycler_view);
        //recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        mContactCardAdapter = new ContactCardAdapter(this);
        recyclerView.setAdapter(mContactCardAdapter);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int prevDy = 0;
            /** Callback method to be invoked when the RecyclerView has been scrolled. This will be
             * called after the scroll has completed.
             *
             * @param recyclerView The RecyclerView which scrolled.
             * @param dx           The amount of horizontal scroll.
             * @param dy           The amount of vertical scroll.
             */
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //Log.d(TAG, "Bottom of toolbar: " + String.valueOf(toolbar.getBottom()) + ", dy: " + String.valueOf(dy));
                // This will collapse the header when scrolling, until its height reaches the toolbar height
                //toolbar.setTranslationY(Math.max(0, -dy));

                if (prevDy < 0 && toolbar.getVisibility() == View.GONE) {
                    toolbar.setVisibilityWithAnimation(View.VISIBLE);
                    toolbar.bringToFront();
                }
                else if (prevDy > 0 && toolbar.getVisibility() == View.VISIBLE) {
                    toolbar.setVisibilityWithAnimation(View.GONE);
                }

                prevDy = dy;
                // Scroll ratio (0 <= ratio <= 1)
                // The ratio value is 0 when the header is completely expanded, 1 when it is completely collapsed
                //float offset = 1 - Math.max((float) (-minHeaderTranslation - dy) / -minHeaderTranslation, 0f);

                // Now that we have this ratio, we only have to apply translations, scales,
                // alpha, etc. to the header views

                // For instance, this will move the toolbar title & subtitle on the X axis
                // from its original position when the ListView will be completely scrolled
                // down, to the Toolbar title position when it will be scrolled up.
                //headerTitleView.setTranslationX(toolbarTitleLeftMargin * offset);
                //headerSubtitleView.setTranslationX(toolbarTitleLeftMargin * offset);

                // Or we can make the FAB disappear when the ListView is scrolled
                //toolbar.setAlpha(1 - offset);
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        View decorView = getWindow().getDecorView();
        int uiOptions = decorView.getSystemUiVisibility();
        int newUiOptions = uiOptions;
        newUiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        newUiOptions |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(newUiOptions);

        PreferenceManager.setDefaultValues(this, R.xml.pref_all, false);

        setupToolbar();
    }

    @Override
    public void onBackPressed() {
        boolean handled = mContactCardAdapter.onBackPressed();
        if (!handled) {
            super.onBackPressed();
        }
    }

    private void setupToolbar() {
        toolbar = (AnimatedToolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_ic_launcher);
        toolbar.bringToFront();
    }

    @Override
    protected void onStart() {
        super.onStart();

        ColorChangeReceiver receiver = new ColorChangeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(MainActivity.CHANGE_COLOR_THEME_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        if (mContactCardAdapter != null) mContactCardAdapter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Ensure this group is available so that the user can give us stuff to monitor
        new GroupManager.SetupGroupTask().execute(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will automatically handle clicks on
        // the Home/Up button, as long as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //Log.d(TAG, "Settings action button selected");
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivityForResult(settingsIntent, SETTINGS_REQUEST_CODE);
            return true;
        }
        // TODO: ensure that there are actually messages to backup before opening SAF, if there aren't
        // any, then show a simple dialog explaining this
        else if (id == R.id.action_export) {
            //Log.d(TAG, "Export action button selected");
            onExportDataBaseClick();
            return true;
        }
        else if (id == R.id.action_app_info) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            StringBuilder sb = new StringBuilder();
            sb.append(BuildConfig.FLAVOR + "\n")
                .append(BuildConfig.VERSION_NAME + "\n")
                .append("Build: " + BuildConfig.VERSION_CODE + "\n")
                .append("Type: " + BuildConfig.BUILD_TYPE);
            builder.setMessage(sb.toString())
                    .setTitle(R.string.app_name)
                    .setPositiveButton("Got it!", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        /*else {
            onTestCreatePdf();
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent. Instead, a URI to
            // that document will be contained in the return intent provided to this method as a
            // parameter. Pull that URI using data.getData().
            if (data != null) {
                Uri uri = data.getData();
                new DataBaseHelper.DuplicateMessagesDb(this).execute(uri);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Open a file chooser so the user can choose where to save the messages database
     */
    private void onExportDataBaseClick() {
        // If current build is greater than API19 (KITKAT) then use SAF (Storage Access Framework),
        // otherwise, create a simple file chooser from scratch
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);

            // Filter to only show results that can be "opened", such as a file (as opposed to a list of contacts or timezones).
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);

            // Create a file with the requested MIME type.
            intent.setType(MIMETYPE);
            intent.putExtra(Intent.EXTRA_TITLE, DEFAULT_BACKUP_TITLE + FILE_EXTENSION);
            startActivityForResult(intent, CREATE_FILE_REQUEST_CODE);
        }
        else {
            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            try {
                // TODO create file chooser activity that function like SAF....
                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 1002);
            }
            catch (android.content.ActivityNotFoundException ex) {
                // Potentially direct the user to the Market with a Dialog
                Toast.makeText(this, "Please install a File Manager.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onTestCreatePdf() {
        Log.d(TAG, "Creating *.pdf...");
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(100, 100, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        View content = new RelativeLayout(this); //((ViewGroup)  this.findViewById(android.R.id.content)).getChildAt(0);
        TextView tv = new TextView(this);
        content.draw(page.getCanvas());
        document.finishPage(page);
        // add more pages
        File outFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "testpdf.pdf");
        //outFile.mkdirs();
        try {
            OutputStream output = new FileOutputStream(outFile);
            document.writeTo(output);
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
        document.close();
        Log.d(TAG, "...finished *.pdf");
    }
    
    public void onTestButtonClick(View v) {

        Log.d(TAG, "Test button clicked");
        GroupManager.getCurrentUserEmail(this);
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (am != null) {
            Intent intent = new Intent(this.getApplicationContext(), MonitorService.class);
            PendingIntent pIntent = PendingIntent.getService(getApplicationContext(), SettingsActivity.ALARM_SERVICE_REQUEST_CODE, intent, 0);

            long first =  System.currentTimeMillis();
            am.set(AlarmManager.RTC, first, pIntent);
        }
    }

    private void restartActivity() {
        recreate();
    }

    private class ColorChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context p1, Intent p2) {
            String action = p2.getAction();
            if (action.equals(CHANGE_COLOR_THEME_ACTION)) {
                restartActivity();
            }
        }
    }
}
