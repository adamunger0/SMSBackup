package com.xwhiteknuckle.smsbackup;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This class provides the views to display for the two different lists (contacts or messages). It
 * also provides a mechanism to switch between these different views and to populate these views
 * with the data they are to display.
 */
public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    // The 3 different types of views that this class represents
    public static final int LEFT_MESSAGE_VIEW_HOLDER_ID = 1234;
    public static final int RIGHT_MESSAGE_VIEW_HOLDER_ID = 9876;
    public static final int CONTACT_VIEW_HOLDER_ID = 5678;
    public static final int PADDING_VIEW_HOLDER_ID = 1357;

    protected int _ID;

    // Used to swap out the views as the necessity for this viewholder changes and to access a valid
    // Context.
    protected ViewGroup parent;

    // The currently display rootView containing the children which in turn display each piece of data.
    protected View rootView;

    // If the caller would like to catch click events on this rootView, then it can set this.
    protected RecyclerViewItemClickInterface clickInterface;

    /**
     * Creates a properly initialized instance of ViewHolder
     * @param viewGroup the parent, containing view for this ViewHolder's views
     * @param view the initial view to display
     * @param clickInterface a click interface for returning onClick events
     */
    public ViewHolder(ViewGroup viewGroup, View view, RecyclerViewItemClickInterface clickInterface) {
        super(view);

        int id = getViewIdOrThrow(view);

        setClickInterface(clickInterface);
        // Nothing to do with padding views so don't register a click listener
        if (id != PADDING_VIEW_HOLDER_ID) view.setOnClickListener(this);

        setViewId(id);
        parent = viewGroup;
        rootView = view;
    }

    /**
     * Creates a properly initialized instance of ViewHolder
     * @param viewGroup the parent, containing view for this ViewHolder's views
     * @param view the initial view to display
     */
    public ViewHolder(ViewGroup viewGroup, View view) {
        super(view);

        int id = getViewIdOrThrow(view);

        // Still register a click listener just in case the caller sets a click listener later
        if (id != PADDING_VIEW_HOLDER_ID) view.setOnClickListener(this);

        setViewId(id);
        parent = viewGroup;
        rootView = view;
    }

    /**
     * Helper to determine the id of this view.
     * @param view view to check
     * @return one of: ViewHolder.CONTACT_VIEW_HOLDER_ID
     *                 ViewHolder.MESSAGE_VIEW_HOLDER_ID
     *                 ViewHolder.PADDING_VIEW_HOLDER_ID or
     *                 -1 for an unrecognized view
     */
    public int getViewId(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.contact_card:
                return CONTACT_VIEW_HOLDER_ID;
            case R.id.left_message_card:
                return LEFT_MESSAGE_VIEW_HOLDER_ID;
            case R.id.right_message_card:
                return RIGHT_MESSAGE_VIEW_HOLDER_ID;
            case R.id.padding_card:
                return PADDING_VIEW_HOLDER_ID;
            default:
                return -1;
        }
    }

    protected int getViewIdOrThrow(View view) {
        int id = getViewId(view);
        if (id != CONTACT_VIEW_HOLDER_ID &&
                id != LEFT_MESSAGE_VIEW_HOLDER_ID &&
                id != RIGHT_MESSAGE_VIEW_HOLDER_ID &&
                id != PADDING_VIEW_HOLDER_ID)
            throw new IllegalArgumentException("Invalid view for ViewHolder");

        return id;
    }

    protected void setViewId(int id) {
        if (id != CONTACT_VIEW_HOLDER_ID &&
                id != LEFT_MESSAGE_VIEW_HOLDER_ID &&
                id != RIGHT_MESSAGE_VIEW_HOLDER_ID &&
                id != PADDING_VIEW_HOLDER_ID)
            throw new IllegalArgumentException("Invalid ViewHolder type");
        _ID = id;
    }

    public int getId() {
        return _ID;
    }

    @Override
    public void onClick(View v) {
        if (clickInterface != null) clickInterface.onItemClick(v, getId());
    }

    public void setClickInterface(RecyclerViewItemClickInterface ci) {
        clickInterface = ci;
    }

    public RecyclerViewItemClickInterface getClickInterface() {
        return clickInterface;
    }

    public void setView(int newId) {
        if (newId != _ID) {
            if (parent == null)
                throw new IllegalStateException("ViewHolder must have a valid ViewGroup");

            setViewId(newId);

            View newView;
            switch (getId()) {
                case LEFT_MESSAGE_VIEW_HOLDER_ID:
                    newView = createLeftMessageView();
                    break;
                case RIGHT_MESSAGE_VIEW_HOLDER_ID:
                    newView = createRightMessageView();
                    break;
                case CONTACT_VIEW_HOLDER_ID:
                    newView = createContactView();
                    break;
                default:
                    newView = createPaddingView();
            }

            newView.setOnClickListener(this);

            final int index = parent.indexOfChild(rootView);

            parent.removeView(rootView);
            parent.addView(newView, index);

            rootView = newView;
        }
    }

    protected View createLeftMessageView() {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.backed_up_message_left_card, (ViewGroup) rootView.getParent(), false);
    }

    protected View createRightMessageView() {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.backed_up_message_right_card, (ViewGroup) rootView.getParent(), false);
    }

    protected View createContactView() {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.backed_up_contact_card, (ViewGroup) rootView.getParent(), false);
    }

    protected View createPaddingView() {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.padding_view, (ViewGroup) rootView.getParent(), false);
    }

    protected void setViewHolderAsLeftMessageView(String snippet, String from, String name, String photoUri, long date) {
        setView(LEFT_MESSAGE_VIEW_HOLDER_ID);
        // Set the most recent message from/to this contact
        ((TextView) rootView.findViewById(R.id.left_message_card_snippet)).setText(snippet);
        // Set the phone number of this contact in a pretty format
        ((TextView) rootView.findViewById(R.id.left_message_card_number)).setText(formatPhoneNumber(from));
        // Set the contacts name
        ((TextView) rootView.findViewById(R.id.left_message_card_name)).setText(name);
        // Set the date and time to this card
        ((TextView) rootView.findViewById(R.id.left_message_card_date_time)).setText(convertLongToStringDateTime(date));
        // Use an AsyncTask to set the image for this contact from the uri if this contact has an
        // image, otherwise display a theme appropriate default icon
        ImageView thumbnail = (ImageView) rootView.findViewById(R.id.left_message_card_thumbnail);
        setPrettyImageFromUriStringToThumbnail(thumbnail, photoUri);
    }

    protected void setViewHolderAsRightMessageView(String snippet, String from, String name, String photoUri, long date) {
        setView(RIGHT_MESSAGE_VIEW_HOLDER_ID);
        // Set the most recent message from/to this contact
        ((TextView) rootView.findViewById(R.id.right_message_card_snippet)).setText(snippet);
        // Set the phone number of this contact in a pretty format
        ((TextView) rootView.findViewById(R.id.right_message_card_number)).setText(formatPhoneNumber(from));
        // Set the contacts name
        ((TextView) rootView.findViewById(R.id.right_message_card_name)).setText(name);
        // Set the date and time to this card
        ((TextView) rootView.findViewById(R.id.right_message_card_date_time)).setText(convertLongToStringDateTime(date));
        // Use an AsyncTask to set the image for this contact from the uri if this contact has an
        // image, otherwise display a theme appropriate default icon
        ImageView thumbnail = (ImageView) rootView.findViewById(R.id.right_message_card_thumbnail);
        setPrettyImageFromUriStringToThumbnail(thumbnail, photoUri);
    }

    protected void setViewHolderAsContactView(String snippet, String from, String name, String photoUri, long date) {
        setView(CONTACT_VIEW_HOLDER_ID);
        // Set the most recent message from/to this contact
        ((TextView) rootView.findViewById(R.id.contact_card_snippet)).setText(snippet);
        // Set the phone number of this contact in a pretty format
        ((TextView) rootView.findViewById(R.id.contact_card_number)).setText(formatPhoneNumber(from));
        // Set the contacts name
        ((TextView) rootView.findViewById(R.id.contact_card_name)).setText(name);
        // Use an AsyncTask to set the image for this contact from the uri if this contact has an
        // image, otherwise display a theme appropriate default icon
        ImageView thumbnail = (ImageView) rootView.findViewById(R.id.contact_card_thumbnail);
        setPrettyImageFromUriStringToThumbnail(thumbnail, photoUri);
    }

    protected void setViewHolderAsPaddingView() {
        //setView(PADDING_VIEW_HOLDER_ID);
    }

    protected String convertLongToStringDateTime(long secondsSinceEpoch) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(secondsSinceEpoch);
        Date d = c.getTime();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a, MMMM dd yyyy");
        return format.format(d);
    }

    @SuppressLint("NewApi")
    protected String formatPhoneNumber(String rawNumber) {
        rawNumber = PhoneNumberUtils.formatNumber(rawNumber, "CA");
        if (rawNumber != null) {
            rawNumber = rawNumber.replace("+", "");
            rawNumber = rawNumber.substring(2);
        }
        return rawNumber;
    }

    protected void setPrettyImageFromUriStringToThumbnail(View thumbView, String photoUri) {
        if (thumbView instanceof ImageView) {
            ImageView thumbnail = (ImageView) thumbView;
            if (photoUri != null && photoUri.length() > 0) {
                Uri uri = Uri.parse(photoUri);
                if (cancelPotentialWork(uri, thumbnail)) {
                    final BitmapWorkerTask task = new BitmapWorkerTask(thumbnail);
                    thumbnail.setImageDrawable(new AsyncDrawable(parent.getContext().getResources(), BitmapFactory.decodeResource(parent.getContext().getResources(), android.R.drawable.gallery_thumb), task));
                    task.execute(uri);
                }
            }
            else {
                String theme = ThemeSwitcher.getTheme(parent.getContext());
                switch (theme) {
                    case "Blue theme":
                        thumbnail.setImageResource(R.drawable.ic_default_user_pic_blue_theme);
                        break;
                    case "Green theme":
                        thumbnail.setImageResource(R.drawable.ic_default_user_pic_green_theme);
                        break;
                    case "Yellow theme":
                        thumbnail.setImageResource(R.drawable.ic_default_user_pic_yellow_theme);
                        break;
                }
            }
        }
    }

    /**
     * An AsyncTask that will load the user's profile image asynchronously. The contacts cursor contains
     * a column for the image URI. This uri then needs to be loaded from memory and cropped and adjusted
     * for a circular display. This adds noticeable lag to the scrolling of this RecyclerView, so this
     * AsyncTask exists to offload all that processing off of the main UI thread on which this RecyclerView
     * runs. If the view that this AsyncTask is loading an image for is in line to be GC'd, the
     * weak reference allows the GC'er to do it's thing.
     */
    class BitmapWorkerTask extends AsyncTask<Uri, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;
        private Uri data = null;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<>(imageView);
        }
        // Decode image in background
        @Override
        protected Bitmap doInBackground(Uri... params) {
            data = params[0];
            Bitmap unprocessedBitmap = ImageHelper.getBitmapFromUri(parent.getContext(), data);
            final int radius = Math.round(ImageHelper.convertDpToPixel(parent.getContext(), 100));
            return ImageHelper.getRoundBitmap(unprocessedBitmap, radius);
        }
        // Once complete, see if ImageView is still around and set bitmap if it is.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (imageViewReference == null || imageViewReference.get() == null || isCancelled()) {
                bitmap = null;
            }
            // TODO: horrible if statement!!!!!!
            if (bitmap != null && (imageViewReference.get().getVisibility() == View.VISIBLE || imageViewReference.get().getVisibility() == View.INVISIBLE)) {
                final ImageView imageView = imageViewReference.get();
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    /**
     * This class extends BitmapDrawable so can be passed as a Drawable. This class holds a weak
     * reference to the AsyncTask that is doing the actual loading of the bitmap.
     */
    static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;
        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }
        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    /**
     * Method to cancel the asynchronous loading of a bitmap if the containing view no longer
     * exists. First this will obtain a handle to the AsyncTask doing the loading, then will check that
     * the input uri (the current photo uri) differs from what this task was loading, if the current
     * uri and the task's uri are the same then there is nothing to do - this view is already loading
     * the right picture for this view's contact. If they aren't the same, then this method will cancel
     * that task so that a new AsyncTask can be started for this view. Canceling the task in practice
     * will likely have very little effect since it is up to the implementation to check for isCancelled()
     * in doInBackground() and return from there, but our implementation is a one-liner so not much to do.
     * @param uri location of contact photo for the contact that the container view represents
     * @param imageView the image view to host the final bitmap from uri
     * @return true if an AsyncTask was cancelled or non-existent, or false if an AsyncTask is already
     * performing the correct task.
     */
    private static boolean cancelPotentialWork(Uri uri, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final Uri bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == null || !bitmapData.equals(uri)) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            }
            else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    /**
     * Tunnel down through the rabbit hole(s) to get the AsyncTask that is loading this ImageView's
     * bitmap. ImageView -> AsyncDrawable -> BitmapWorkerTask
     * @param imageView get the worker task from this ImageView
     * @return a BitmapWorkerTask or null
     */
    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }
}