package com.xwhiteknuckle.smsbackup;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.preference.PreferenceManager;


public class ThemeSwitcher {
    public static void onActivityCreateSetTheme(Activity activity) {
        String theme = getTheme(activity);

        if (theme.equals("Blue theme")) {
            activity.setTheme(R.style.BlueAppTheme);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.primary_darker_color_blue_theme));
        }
        else if (theme.equals("Green theme")) {
            activity.setTheme(R.style.GreenAppTheme);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.primary_darker_color_green_theme));
        }
        else if (theme.equals("Yellow theme")) {
            activity.setTheme(R.style.YellowAppTheme);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.primary_darker_color_yellow_theme));
        }
    }

    public static String getTheme(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("pref_theme", "Green theme");
    }
}
