-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:5:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:tools
		ADDED from AndroidManifest.xml:4:5
	xmlns:android
		ADDED from AndroidManifest.xml:3:5
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.READ_PROFILE
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.WRITE_CONTACTS
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.READ_SMS
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:18:5
	android:name
		ADDED from AndroidManifest.xml:18:22
application
ADDED from AndroidManifest.xml:22:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:cardview-v7:21.0.3:16:5
MERGED from com.android.support:gridlayout-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:recyclerview-v7:21.0.3:17:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from SMSBackup:animatedtoolbar:unspecified:12:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:25:9
	android:allowBackup
		ADDED from AndroidManifest.xml:23:9
	android:icon
		ADDED from AndroidManifest.xml:24:9
	android:theme
		ADDED from AndroidManifest.xml:26:9
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:29:9
MERGED from com.google.android.gms:play-services:6.5.87:21:9
	android:value
		ADDED from AndroidManifest.xml:30:13
	android:name
		ADDED from AndroidManifest.xml:29:20
activity#com.xwhiteknuckle.smsbackup.MainActivity
ADDED from AndroidManifest.xml:32:9
	android:label
		ADDED from AndroidManifest.xml:34:13
	android:name
		ADDED from AndroidManifest.xml:33:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:35:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:36:17
	android:name
		ADDED from AndroidManifest.xml:36:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:37:17
	android:name
		ADDED from AndroidManifest.xml:37:27
activity#com.xwhiteknuckle.smsbackup.SettingsActivity
ADDED from AndroidManifest.xml:41:9
	android:label
		ADDED from AndroidManifest.xml:43:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:44:13
	tools:ignore
		ADDED from AndroidManifest.xml:45:13
	android:name
		ADDED from AndroidManifest.xml:42:13
meta-data#android.support.PARENT_ACTIVITY
ADDED from AndroidManifest.xml:46:13
	android:value
		ADDED from AndroidManifest.xml:48:17
	android:name
		ADDED from AndroidManifest.xml:47:17
intent-filter#android.intent.action.MANAGE_NETWORK_USAGE+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:49:13
action#android.intent.action.MANAGE_NETWORK_USAGE
ADDED from AndroidManifest.xml:50:17
	android:name
		ADDED from AndroidManifest.xml:50:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:51:17
	android:name
		ADDED from AndroidManifest.xml:51:27
intent-filter#android.intent.action.MANAGE_BATTERY_USAGE+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:53:13
action#android.intent.action.MANAGE_BATTERY_USAGE
ADDED from AndroidManifest.xml:54:17
	android:name
		ADDED from AndroidManifest.xml:54:25
service#com.xwhiteknuckle.smsbackup.MonitorService
ADDED from AndroidManifest.xml:59:9
	android:exported
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:59:18
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:cardview-v7:21.0.3:15:5
MERGED from com.android.support:gridlayout-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:recyclerview-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from SMSBackup:animatedtoolbar:unspecified:8:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
activity#android.support.v7.widget.TestActivity
ADDED from com.android.support:recyclerview-v7:21.0.3:18:9
	android:label
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:19
	android:name
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:60
