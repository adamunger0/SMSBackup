package com.xwhiteknuckle.animatedtoolbar;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;

public class AnimatedToolbar extends Toolbar {

    // Default to no animation if caller hasn't specified anything
    public int slideInDuration = 0;
    public int slideOutDuration = 0;
    public float decelerateInFactor = 0.0f;
    public float decelerateOutFactor = 0.0f;
    
    // The default animations to use when toggling toolbar visibility
    public ObjectAnimator alphaOutAnimator;
    public ObjectAnimator alphaInAnimator;
    
    
    // Will use this to prevent animations from suddenly starting stopping
    public boolean animationInProgress = false;

    // Save the visibility request so we can call this.setVisibility(visibility) from this.onAnimationEnd(Animation animation)
    protected int visibility;
    
    //Context context;

    /**
     * 
     */
    public AnimatedToolbar(Context c) {
        super(c);
        init(c, null);
    }

    public AnimatedToolbar(Context c, AttributeSet attrs) {
        super(c, attrs);
        init(c, attrs);
    }

    public AnimatedToolbar(Context c, AttributeSet attrs, int defStyleAttr) {
        super(c, attrs, defStyleAttr);
        init(c, attrs);
    }

    /**
     * Setup the initial parameters of this toolbar
     */
    protected void init(Context c, AttributeSet attrs) {
        
        // setLayerType(int type, Paint paint) is only available from >= API 11
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        
        //context = c;
        
        // Get all of the XML defined parameters
        if (attrs != null) {
            TypedArray a = c.obtainStyledAttributes(attrs, R.styleable.AnimatedToolbar); //android.R.integer.config_shortAnimTime
            slideInDuration = a.getResourceId(R.styleable.AnimatedToolbar_slideInDuration, slideInDuration);
            if (slideInDuration != 0) slideInDuration = c.getResources().getInteger(slideInDuration);
            slideOutDuration = a.getResourceId(R.styleable.AnimatedToolbar_slideOutDuration, slideOutDuration);
            if (slideOutDuration != 0) slideOutDuration = c.getResources().getInteger(slideOutDuration);
            decelerateInFactor = a.getFloat(R.styleable.AnimatedToolbar_decelerateInFactor, decelerateInFactor);
            decelerateOutFactor = a.getFloat(R.styleable.AnimatedToolbar_decelerateOutFactor, decelerateOutFactor);
            a.recycle();
        }
        
        // Create alpha fader animators
        createInAlphaAnimation();
        createOutAlphaAnimation();
    }
    
    /**
     * Create the alpha fade-out ObjectAnimator
     */
    protected void createOutAlphaAnimation() {
        DecelerateInterpolator di = new DecelerateInterpolator(decelerateOutFactor);
        alphaOutAnimator = ObjectAnimator.ofFloat(this, "alpha", new float[]{0.0f, 1.0f});
        alphaOutAnimator.setInterpolator(di);
        alphaOutAnimator.setDuration(slideOutDuration);
    }
    
    /**
     * Create and return an animator for sliding this toolbar onto the screen
     */
    protected Animation getOutAnimation() {
        DecelerateInterpolator di = new DecelerateInterpolator(decelerateOutFactor);
        Animation outAnimation = new TranslateAnimation(0.0f, 0.0f, -this.getHeight(), 0.0f);
        outAnimation.setInterpolator(di);
        outAnimation.setDuration(slideOutDuration);
        
        return outAnimation;
    }
    
    /**
     * Create the alpha fade-in ObjectAnimator
     */
    protected void createInAlphaAnimation() {
        DecelerateInterpolator di = new DecelerateInterpolator(decelerateInFactor);
        alphaInAnimator = ObjectAnimator.ofFloat(this, "alpha", new float[]{1.0f, 0.0f});
        alphaInAnimator.setInterpolator(di);
        alphaInAnimator.setDuration(slideInDuration);
    }
    
    /**
     * Create and return an animator for sliding this toolbar off of the screen
     */
    protected Animation getInAnimation() {
        DecelerateInterpolator di = new DecelerateInterpolator(decelerateInFactor);
        Animation inAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, -this.getHeight());
        inAnimation.setInterpolator(di);
        inAnimation.setDuration(slideInDuration);
        
        return inAnimation;
    }

    /**
     * Set the animationInProgress flag so that they can finish uninterrupted
     */
    @Override
    protected void onAnimationStart() {
        //Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
        animationInProgress = true;
        super.onAnimationStart();
    }

    /**
     * Set the animationInProgress flag off so that new animations can begin
     */
    @Override
    protected void onAnimationEnd() {
        //Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
        super.onAnimationEnd();
        animationInProgress = false;
        this.setVisibility(visibility);
    }
    
    /**
     * Toggle this toolbars visibility with slide up and fade out animations
     */
    public void setVisibilityWithAnimation(int vis) {
        // Initiate the animations only if they aren't currently in progress
        if (!animationInProgress && getVisibility() != vis) {
            visibility = vis;
            if (visibility == VISIBLE) {
                this.startAnimation(getOutAnimation());
                if (alphaOutAnimator != null) alphaOutAnimator.start();
            }
            else if (visibility == INVISIBLE || visibility == GONE) {
                this.startAnimation(getInAnimation());
                if (alphaInAnimator != null) alphaInAnimator.start();
            }
        }
    }
}
